#!/usr/bin/env bash

curl -X POST \
http://localhost:8080/invocations \
-H 'Cache-Control: no-cache' \
-H 'Content-Type: application/json' \
-H 'Postman-Token: 41189b9a-40e2-abcf-b981-c31ae692072e' \
-d '{
    "title": "PreCommerjghjghjgcial Procurement of a Mercury Free Method for Analysis of Organic Matter in Wastewater and Waste Products.",
    "description": "Analysis of chemical oxygen demand (COD) in wastewater is used as a measure of effluent total content of organic matter. The Swedish wastewater treatment plants (WWTPs) use a COD analysis method called CODCr analysis in accordance to Swedish Standards SS 028142 that involves the use of both mercury prohibited by law in Sweden and dichromate listed in REACH. CODCr analysis is currently the only reliable method available at the WWTPs for COD analysis. The need to find an alternative COD analysis method is addressed through this precommercial innovation procurement with the goal to find suppliers that can deliver a corresponding analysis method to COD(Cr) which does not involve mercury and preferably not dichromate."
}'