import os
from typing import Dict

import joblib
import numpy as np
import torch
from sentence_transformers import SentenceTransformer, util

_MODEL_PATH = os.path.join('/opt/ml/', 'model')  # Path where all your model(s) live in
THRESHOLD = 0.25


class ModelService(object):
    embeddings_reference = joblib.load(os.path.join(_MODEL_PATH, "embeddings_reference.joblib"))
    labels_reference = joblib.load(os.path.join(_MODEL_PATH, "labels_reference.joblib"))
    all_cpvs = joblib.load(os.path.join(_MODEL_PATH, "all_cpvs.joblib"))
    embedder_l6 = SentenceTransformer('all-MiniLM-L6-v2')
    top_k = 20
    normalized_factor = 0
    for i in range(1, top_k + 1):
        normalized_factor += 1 / i
    print(f"{len(embeddings_reference)=}")
    print(f"{len(labels_reference)=}")
    print(f"{len(all_cpvs)=}")
    print(normalized_factor)

    @staticmethod
    def extract_cpvs_from_full_cpv(local_cpvs, number_digits):
        divisions = []
        local_cpvs = [str(local_cpv) for local_cpv in local_cpvs]
        for local_cpv in local_cpvs:
            if len(local_cpv) < 7:
                continue
            if len(local_cpv) == 7:
                local_cpv = "0" + local_cpv
            for upper_class in range(2, number_digits + 1):
                division = local_cpv[:upper_class]
                length_division = len(division)
                if length_division < number_digits:
                    remaining_digits = number_digits - length_division
                    division = division + "0" * remaining_digits
                if division not in divisions:
                    divisions.append(division)
        return divisions

    def compute_scores(self, results_similarities):
        single_row_pred = [0.0] * len(self.all_cpvs)
        cpv_found_and_scores = {}
        position = 0
        for single_pred in results_similarities:
            position += 1
            pred_values = single_pred["pred"]
            score_pred = single_pred["score"]
            for pred_value in pred_values:
                if not cpv_found_and_scores.get(pred_value):
                    cpv_found_and_scores[pred_value] = score_pred / position
                else:
                    cpv_found_and_scores[pred_value] = cpv_found_and_scores[pred_value] + (
                            score_pred / position)
        for key, value in cpv_found_and_scores.items():
            index_cpv = self.all_cpvs.index(key)
            normalized_score = value / self.normalized_factor
            single_row_pred[index_cpv] = normalized_score
        return np.array(single_row_pred).astype(float)

    def predict(self, input_text):
        results = []
        query_embedding = self.embedder_l6.encode(input_text, convert_to_tensor=True)
        cos_scores = util.cos_sim(query_embedding, self.embeddings_reference)[0]
        top_results = torch.topk(cos_scores, k=self.top_k)
        list_of_closest_notices = []
        for score, idx in zip(top_results[0], top_results[1]):
            cpvs_closest_notice = self.labels_reference[int(idx)]
            list_of_closest_notices.extend(cpvs_closest_notice)
            results.append({"pred": cpvs_closest_notice, "score": score.tolist()})
        print(f"Top 20 closest notices with cpvs: {results}")
        return self.compute_scores(results)


MODEL = ModelService()


def predict(json_input: Dict):
    """
    Prediction given the request input
    :param json_input: [dict], request input
    :return: [dict], prediction
    """
    print(f"Predict for JSON input: {json_input}")

    title = json_input.get("title", "")
    description = json_input.get("description", "")
    if not title and not description:
        raise Exception("No title and description provided")

    preprocessed_title_description = f"{title} {description}"
    predictions = MODEL.predict(preprocessed_title_description)
    results = []
    for index, score in enumerate(predictions):
        if score > THRESHOLD:
            cpv_number = MODEL.all_cpvs[index]
            results.append({
                "cpv_number": cpv_number,
                "score": score
            })

    print(f"Return results: {results}")
    return {
        "predictions": results
    }
